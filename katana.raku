#!/usr/bin/env raku

use v6.d;
use lib 'lib';

use Katana::Exec::Command;
use Katana::HTML::Page;
use Katana::HTML::Tag;
use Katana::Image::Elem;
use Katana::Walk::Dir;

sub camera-stats(@images) {
  my %cameras;
  %cameras{.camera}++ for @images;
  return %cameras.sort(*.value).reverse;
}

multi MAIN(
  Bool :$mr-proper,             #= Clean output dir
  Str :$in-dir = './in',        #= Input dir
  Str :$dist-dir = './dist',    #= Output dir
  Int :$thumb-geometry = 800,   #= Thumbnail geometry
  Str :$bg-blur = '0x8',        #= Background blur factor
  Bool :$randomize = True,      #= Randomize order of images
  Str :$title = 'Katana album', #= Album title
  Int :$degree = 4,             #= Degree of parallelism
) {

  my @body-tags = (Tag.new: :name<hr>, :is-mono(True)); 
  my Katana::HTML::Page \body .= new: :@body-tags, :$title;
  body.generate;
  exit 0;
  my @images = dir($in-dir, test => { "$in-dir/$_".IO.f }).map:{
    Elem.new: source => $_, :$dist-dir
  };
  say "Found {@images.elems} images";

  @images = @images.pick: * if $randomize;
  dir-cleanup-nonexistent $dist-dir, @images;
  dir-make-mr-proper $dist-dir if $mr-proper;
  dir-ensure $dist-dir;

  @images.race(:$degree).map: {
    .get-camera-model;
    .generate: :$thumb-geometry, :$bg-blur;
  };

  .say for camera-stats @images;
}
