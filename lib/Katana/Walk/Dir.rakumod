use v6.d;
use Katana::Image::Elem;
unit module Katana::Walk::Dir:api<1>;

sub dist-dirs(Str \dist-dir --> List) { dist-dir <<~>> </large /blur /thumb> }

sub dir-ensure(Str \dist-dir) is export {
  mkdir dist-dir unless dist-dir.IO.d;
  mkdir $_ unless .IO.d for dist-dirs dist-dir;
}

sub dir-walk(Str \dir, :&onFile, :&onDir = sub (\dir) {} ) is export {
  return unless dir.IO.d;
  for dir(dir) -> $fh {
    given $fh {
      when .d { dir-walk $fh.path, :&onFile, :&onDir }
      default { &onFile($fh.path) }
    }
  }
  &onDir(dir);
}

sub dir-cleanup-nonexistent (Str \dist-dir, @images) is export {
  my $basenames = set @images.map:{ $_.basename };
  dir-walk dist-dir,
    onFile => sub (\file) {
      unlink file if file.IO.basename ∉ $basenames
    };
}

sub dir-make-mr-proper(Str \dist-dir) is export {
  dir-walk dist-dir,
    onFile => sub (\file) { unlink file },
    onDir => sub (\dir) { rmdir dir };
}

