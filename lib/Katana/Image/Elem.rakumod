use v6.d;
use Katana::Exec::Command;
unit class Katana::Image::Elem:api<1> is export;

has Str $.basename;
has Str $!source;
has Str $!dist-dir;
has Str $.camera;

submethod BUILD(IO::Path :$source, Str :$dist-dir) {
  $!basename = $source.basename;
  $!source = $source.path;
  $!dist-dir = $dist-dir;
}

method generate(Int :$thumb-geometry, Str :$bg-blur) {
  my $thumb = "$!dist-dir/thumb/{$.basename}";
  ⁉ [|<convert -auto-orient -geometry>, $thumb-geometry, $!source, $thumb];

  my $blur = "$!dist-dir/blur/{$.basename}";
  ⁉ [|<convert -flip -geometry>, $thumb-geometry/4, '-blur', $bg-blur, $thumb, $blur];

  my $large = "$!dist-dir/large/{$.basename}";
  ⁉ ['cp', $!source, $large];
}

method get-camera-model returns Str {
  my %exif = read-exif-info "$!dist-dir/large/{$.basename}";
  my $make = %exif<Make> ?? %exif<Make> !! '';
  my $model = %exif{'Camera Model Name'} ?? %exif{'Camera Model Name'} !! '';
  $model ~~ s/$make //; # Don't stotter
  $model = 'Unknown camera' if $model eq '';
  $!camera = "$make $model".trim;
}
