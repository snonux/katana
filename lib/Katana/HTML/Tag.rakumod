use v6.d;
unit class Katana::HTML::Tag is export;

has Str $.name;
has Str $.text;
has Str %.params;
has Bool $.is-mono = False;
has Katana::HTML::Tag @.succ;

method has-text returns Bool { defined $.text }
method has-succ returns Bool { defined $.succ }

method open returns Str {
  return "<{$.name}{self.params} />" if $!is-mono;
  return "<{$.name}{self.params}>"
}
method close returns Str { "</{$.name}>" }

method params returns Str {
  return '' unless defined %!params;
  my @params;
  for %!params.kv -> $key, $val {
    push @params, " $key='$val'";
  }
  return @params.join;
}
