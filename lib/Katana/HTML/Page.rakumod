use v6.d;
use Katana::HTML::Tag;
unit class Katana::HTML::Page:api<1> is export;

has Str $!doctype = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
has @!body-tags is required;
has Str $!title is required;

submethod BUILD(:@!body-tags where *.elems > 0, Str :$!title where * ne '') { }

method generate {
  my @tags;
  push @tags, Tag.new: name => 'html',
    params => (
      xmlns => 'http://www.w3.org/1999/xhtml',
      lang => 'en',
      'xml:lang' => 'en'
    );

  push @tags, Tag.new: name => 'head', succ => @!body-tags;
  push @tags, Tag.new: name => 'title', text => $!title;

  say $!doctype;
  self.recurse(@tags);
}

method recurse(@tags) {
  my @sgat; # tags spelled in reverse.
  for @tags -> $tag {
    say $tag.open;
    say $tag.text if $tag.has-text;
    unshift @sgat, $tag;
  }
  for @sgat -> $tag {
    say $tag.close unless $tag.is-mono;
    self.recurse($tag.succ) if $tag.has-succ;
  }
}

