use v6.d;
unit module Katana::Exec::Command:api<1>;

sub prefix:<❱>(*@args) is export {
  say "❱ {@args}";
  my \proc = run @args, :out, :err;
  .say if .chars > 0 for proc.out.slurp(:close), proc.err.slurp(:close);
}

sub prefix:<⁉>(*@args) is export { ❱ @args unless @args[*-1].IO.f }

sub read-exif-info(Str $file) is export {
  my %exif;
  my \proc = run 'exiftool', $file, :out;
  for proc.out.slurp(:close).split("\n") -> $line {
    my ($key, $val) = $line.split(':')[0,1];
    next unless defined $val;
    %exif{$key.trim} = $val.trim;
  }
  return %exif;
}

